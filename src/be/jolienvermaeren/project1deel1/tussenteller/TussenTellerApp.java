package be.jolienvermaeren.project1deel1.tussenteller;

public class TussenTellerApp {
    public static void main(String[] args) {
        int a = Operations.readNumber();
        int b = Operations.readNumber();
        Operations.printSum(a, b);

        int product = 1;
        int sum = 0;
        if (a < b) {
            while (a < b) {
                product *= ++a;
                ++a;
            }
            System.out.println("The product of the numbers in between the given numbers is " + product);
        }else if(a > b) {
            while (a > b) {
                product *= ++b;
                ++b;
            }
            System.out.println("The product of the numbers in between the given numbers is " + product);
        }
    }
}
