package be.jolienvermaeren.project1deel1.tussenteller;
import java.util.Scanner;

public class Operations {

    public static int readNumber(){
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Enter a number");
        int number = keyboard.nextInt();
        return number;
    }

    public static void printSum(int num1, int num2){
        int sum = num1 + num2;
        System.out.println(num1 + " + " + num2 + " = " + sum);
    }


}
