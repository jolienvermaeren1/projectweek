package be.jolienvermaeren.project1deel1.unitconverter;
import java.util.Scanner;

public class ConverterApp {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);

        for(int i = 0; i >= 0; i++ ) {
                UnitConverter.showConverterMenu();
                int choice = keyboard.nextInt();
            if (choice == 1) {
                System.out.print("Geef een bedrag in (euros) of voer 0 in om te stoppen: ");
                double euro = keyboard.nextDouble();
                if(euro > 0) {
                    double dollar = euro * 1.22;
                    System.out.println(euro + " euros is gelijk aan " + dollar + " dollars.");
                }else if (euro == 0){
                    System.out.println("Tot de volgende keer!");
                    break;
                }
            } else if (choice == 2) {
                System.out.println("Geef een temperatuur (°C) of voer 0 in om te stoppen: ");
                double celcius = keyboard.nextDouble();
                if(celcius > 0) {
                    double fahrenheit = celcius * 1.8 + 32;
                    System.out.println(celcius + "°C is gelijk aan " + fahrenheit + " °F.");
                } else if (celcius == 0){
                    System.out.println("Tot de volgende keer!");
                    break;
                }
            } else if (choice == 3) {
                System.out.println("Geef een afstand (in meters) of voer 0 in om te stoppen: ");
                double meter = keyboard.nextDouble();
                if (meter > 0) {
                    double feet = meter * 3.28;
                    System.out.println(meter + " meters is gelijk aan " + feet + " feet.");
                } else if (meter == 0){
                    System.out.println("Tot de volgende keer!");
                    break;
                }
            } else if (choice == 4) {
                System.out.print("Geef een afstand (in kilometers) of voer 0 in om te stoppen: ");
                double kilometres = keyboard.nextDouble();
                if(kilometres > 0) {
                    double miles = kilometres * 0.62137;
                    System.out.println(kilometres + " kilometers is gelijk aan " + miles + " mijl.");
                } else if (kilometres == 0){
                    System.out.println("Tot de volgende keer!");
                    break;
                }
            } else if(choice > 4){
            } else if (choice == 0) {
                System.out.println("Tot de volgende keer!");
                break;
            }
        }


    }

}
