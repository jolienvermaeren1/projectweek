package be.jolienvermaeren.project1deel1.computeralphabet;

public class ComputerAlpabetApp {
    public static void main(String[] args) {
        for(char letter = 'A'; letter <= 'Z'; letter++) {
            int ascii = letter;
            System.out.println(letter + " : " + ascii + " : " + Integer.toBinaryString(ascii) + " : " + Integer.toHexString(ascii));
        }

    }
}
