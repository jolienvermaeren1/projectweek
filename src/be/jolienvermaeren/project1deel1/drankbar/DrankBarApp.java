package be.jolienvermaeren.project1deel1.drankbar;
import java.util.Scanner;

// TODO:  FOUTMELDINGEN

public class DrankBarApp {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);

        System.out.println("Voer uw naam in: ");
        String name = keyboard.nextLine();

        System.out.println("Voer uw leeftijd in: ");
        int age = keyboard.nextInt();

        int count = 0;
        for(int i = 0;count >= 0; i++) {
            Menu.showMenu();
            int drink = keyboard.nextInt();

            if (drink == 1) {
                System.out.println("Hier is uw glas water, " + name + ". Smakelijk!");
                ++count;

            } else if (drink == 2) {
                System.out.println("Hier is uw glas cola, " + name + ". Smakelijk!");
                ++count;
            } else if (drink == 3) {
                System.out.println("Hier is uw koffie met een koekje, " + name + ". Smakelijk!");
                ++count;
            } else if (drink == 4) {
                if (age < 16) {
                    System.out.println(name + ", u bent nog te jong voor een biertje!");
                    ++count;
                } else {
                    System.out.println("Hier is uw biertje, " + name + ". Smakelijk!");
                }
            } else if (drink == 0) {
                System.out.println("U bestelde " + count + " drankjes.");
                System.out.println("Tot de volgende keer!");
                break;
            }
        }
    }
}

